certbot
=========

Install certbot, and any plugins required, manages certificates for domains. You should probably not use it, and go for those listed on Galaxy, like [https://galaxy.ansible.com/geerlingguy/certbot](geerlingguy's certbot) role.  This was used internally, and is simple enough for most of our deployments.

Requirements
------------

This probably requires nginx to be installed, though to avoid a strict dependency, there is no specific galaxy role set for this. This is hosted on gitlab.com, so you'll need a specific requirements.yml!

```
---
roles:
  - name: gilou.cloudimg_fixer
    src: https://gitlab.com/wolface/ansible-roles/certbot.git
```


Role Variables
--------------

```
# Plugins to install along
certbot_plugins:
  - python-certbot-nginx
# Email for Let's Encrypt
certbot_email: admin@example.com
# Domains to request a certificate for
certbot_domains:
  - adomain.example.com
  - anotherdomain.example.com
# Use staging environment to avoid rate limits on production acme
certbot_staging: false
```

Example Playbook
----------------

    - hosts: servers
      roles:
         - role: gilou.certbot
           certbot_email: myemail@example.com
           certbot_domains:
             - mydomain.example.com

License
-------

WTFPL

Author Information
------------------

Written by [Gilles Pietri](https://gilouweb.com) for [Wolface.IT](https://www.wolface.fr)
